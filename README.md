Voilà le dossier pour la simulation

Pour la faire marcher, il faut d'abord exécuter le fichier 'setup' avec les bons paramètres.

Il faut régler :

- les positions initiales : x0, y0, psi0
- La résolution de la carte
- la carte désirée dans image = imread('Map')
- la vitesse maximum de la voiture vmax
- l'angle de braquage maximum steer_max
- la durée de simulation Sim_duration


Ensuite, il suffit d'exécuter SimulationAvecAffichage.
L'animation devrait apparaitre au bout d'une minute.
